#ifndef TASKCOMPLETED_H
#define TASKCOMPLETED_H
#include <QWidget>
#include "daytask.h"
namespace Ui {
    class WTasksStub;
}

class WTasksStub : public QWidget {
        Q_OBJECT

    public:
        explicit WTasksStub(QWidget *parent = 0);
        ~WTasksStub();
        void setState(int inState);

    private:
        Ui::WTasksStub *ui;
};


#endif // TASKCOMPLETED_H
