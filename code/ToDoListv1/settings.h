#ifndef SETTINGS_H
#define SETTINGS_H

#include "st.include.h"
#include "container.h"
#include "editsub.h"

namespace Ui {
    class WSettingSchedule;
}

class WSettingSchedule : public QWidget {
        Q_OBJECT

    signals:
        void scheduleChanged();
        void needHelp();

    public:
        explicit WSettingSchedule(QWidget *parent = 0);
        ~WSettingSchedule();
        void enableFirstLaunchMode();
        void enableSmallScreenSupport();

    protected:
        void showEvent(QShowEvent *inSE);

    private:
        void addSubjectInList(STSubject *inSubject);
        void clearSubjectList();

        Ui::WSettingSchedule *ui;
        DEditSubject *editSubject;

    private slots:
        void fillSubjectList();

  //      void setDragSubject(int inRow);

        void sendScheduleChanged();
        void helpRequested();
};

#endif // SETTINGS_H
