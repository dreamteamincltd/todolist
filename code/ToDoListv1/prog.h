#ifndef PROG_H
#define PROG_H

#include "st.include.h"
namespace Ui{
    class ProgressBar;
}
class ProgressBar : public QFrame
{
    Q_OBJECT

//signals:
//   void RefreshProg(int points);
public:
    explicit ProgressBar(QWidget *parent = 0);
    ~ProgressBar();
    void resizeEvent(QResizeEvent *inRE);
public slots:
    void slideToHide();
    void slideToShow();
    void fillBar();
private:
    Ui::ProgressBar * ui;
    QTimer endResizeTimer;
};
/*
class Bar : public QProgressBar
{
    Q_OBJECT
public:
    Bar(QWidget *pwgd = 0);
    ~Bar();
    QProgressBar *bar;
//public slots:
//    void fillbar();

};
*/
#endif // PROG_H
