#include "taskcompleted.h"
#include "ui_taskcompleted.h"

WTasksStub::WTasksStub(QWidget *parent)
    : QWidget(parent), ui(new Ui::WTasksStub) {
    ui->setupUi(this);
}

WTasksStub::~WTasksStub() { delete ui; }

void WTasksStub::setState(int inState) {
    switch (inState) {
        case STATE_CALENDAR: {
            ui->lbStubTitle->setText("Задач на неделю нет");
            ui->lbStubText->setText("или отображение заблокировано фильтром");

            break;
        }
        case STATE_OVERDUE: {
            ui->lbStubTitle->setText("Все задачи выполнены");
            ui->lbStubText->setText("Кейдж гордится тобой!");
            break;
        }
        case STATE_ALL: {
            ui->lbStubTitle->setText("Задач не найдено");
            ui->lbStubText->setText("или отображение заблокировано фильтром");
            break;
        }
    }
}
