#include "editsub.h"
#include "ui_editsub.h"

DEditSubject::DEditSubject(QWidget *parent)
    : QDialog(parent), ui(new Ui::DEditSubject) {
    ui->setupUi(this);

    // Select only one item
    ui->twSubjects->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->twSubjects->setSelectionMode(QAbstractItemView::SingleSelection);

    ui->twSubjects->installEventFilter(this);

    ui->twSubjects->setObjectName("twDialog");
    ui->twSubjects->verticalHeader()->hide();

    ui->twSubjects->setColumnWidth(0, 41);
    ui->twSubjects->setColumnWidth(1, 170);
    ui->twSubjects->setColumnWidth(2, 280);

    setWindowFlags(Qt::Dialog | Qt::WindowTitleHint);

    ui->twSubjects->hideColumn(0);
    ui->pbDelRightLight->setChecked(false);

    connect(ui->pbReady, SIGNAL(clicked()), SLOT(close()));
    connect(ui->pbAddLeftLight, SIGNAL(clicked()), SLOT(addSubject()));
    connect(ui->pbDelRightLight, SIGNAL(toggled(bool)), SLOT(showDelete(bool)));
}

DEditSubject::~DEditSubject() {
    clearTable();

    delete ui;
}

void DEditSubject::showEvent(QShowEvent *inSE) {
    fillTable();

    ui->twSubjects->resizeRowsToContents();

    inSE->accept();
}

void DEditSubject::closeEvent(QCloseEvent *inCE) {
    disableTableEditMode();

    QSettings settings;
    if (isHaveFakeSubjects() && settings.value(SET_DELETE_ITEM).toBool()) {
        int rc = QMessageBox::warning(
                    this, QString::fromUtf8("Ошибка"),
                    QString::fromUtf8(
                        "Все типы заданий с пустым полем отображемого названия будут удалены, "
                        "продолжить?"),
                    QMessageBox::Yes | QMessageBox::No | QMessageBox::YesToAll);
        if (rc == QMessageBox::No) {
            inCE->ignore();
            return;
        }
        if (rc == QMessageBox::YesToAll) {
            settings.setValue(SET_DELETE_ITEM, false);
        }
    }

    inCE->accept();
}

bool DEditSubject::eventFilter(QObject *inObject, QEvent *inEvent) {
    if (inEvent->type() != QEvent::KeyPress) {
        return false;
    }
    QKeyEvent *keyEvent = NULL;
    keyEvent = static_cast<QKeyEvent *>(inEvent);

    QList<QTableWidgetItem *> selectedItems = ui->twSubjects->selectedItems();
    if (selectedItems.size() != 1) {
        return false;
    }
    QTableWidgetItem *item = selectedItems.first();

    int column = item->column();
    int row = item->row();

    if (keyEvent->key() == Qt::Key_Down) {
        row++;
    }
    if (keyEvent->key() == Qt::Key_Up) {
        row--;
    }

    switch (keyEvent->key()) {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        case Qt::Key_Up:
        case Qt::Key_Down: {
            disableTableEditMode();

            ui->twSubjects->setCurrentCell(row, column);
            ui->twSubjects->editItem(ui->twSubjects->item(row, column));
            keyEvent->accept();
            return true;
        }
        default: { break; }
    }
    return false;
}

bool DEditSubject::isHaveFakeSubjects() {
    STSubject *subject = NULL;
    QVariant var;
    for (int i = 0; i < ui->twSubjects->rowCount(); i++) {
        var = ui->twSubjects->item(i, 0)->data(Qt::UserRole);
        subject = var.value<STSubject *>();
        if (subject->id == INVALID_ID || subject->nick == "") {
            return true;
        }
    }

    return false;
}

void DEditSubject::clearTable() {
    ui->twSubjects->disconnect();

    QVariant var;
    STSubject *del = NULL;
    while (ui->twSubjects->rowCount() > 0) {
        delete ui->twSubjects->cellWidget(0, 0);

        var = ui->twSubjects->item(0, 0)->data(Qt::UserRole);
        del = var.value<STSubject *>();
        delete del;

        ui->twSubjects->removeRow(0);
    }
}

void DEditSubject::addSubjectInTable(STSubject *inSubject) {
    disableTableEditMode();

    QPushButton *pb;
    QTableWidgetItem *item;
    QVariant var;

    int index = ui->twSubjects->rowCount();
    ui->twSubjects->insertRow(index);

    pb = new QPushButton;
    pb->setFixedSize(40, 24);
    pb->setAutoDefault(false);
    pb->setObjectName("pbDelMiddleDark");
    connect(pb, SIGNAL(clicked()), SLOT(delSubject()));
    ui->twSubjects->setCellWidget(index, 0, pb);

    item = new QTableWidgetItem();
    var.setValue(inSubject);
    item->setData(Qt::UserRole, var);
    ui->twSubjects->setItem(index, 0, item);

    item = new QTableWidgetItem();
    item->setText(inSubject->nick);
    var.setValue(&inSubject->nick);
    item->setData(Qt::UserRole, var);
    QBrush brsh(QColor(EMPTY_COLOR));
    item->setBackground(brsh);
    ui->twSubjects->setItem(index, 1, item);
    if (inSubject->id == INVALID_ID) {
        // If it's new pair edit them
        ui->twSubjects->editItem(item);
        ui->twSubjects->setCurrentItem(item);
    }

    item = new QTableWidgetItem();
    item->setText(inSubject->name);
    var.setValue(&inSubject->name);
    item->setData(Qt::UserRole, var);
    ui->twSubjects->setItem(index, 2, item);
}

void DEditSubject::fillTable() {
    clearTable();

    QVector<STSubject *> *subjects = STDataProvider::getInstance().getSubjects();
    for (int i = 0; i < subjects->size(); i++) {
        addSubjectInTable((*subjects)[i]);
    }
    delete subjects;

    connect(ui->twSubjects, SIGNAL(itemChanged(QTableWidgetItem *)),
            SLOT(editSubject(QTableWidgetItem *)));
}

void DEditSubject::disableTableEditMode() {
    // This code need for disable edit mode
    QTableWidgetItem *item = ui->twSubjects->currentItem();

    ui->twSubjects->setDisabled(true);
    ui->twSubjects->setDisabled(false);

    if (item) {
        editSubject(item);
    }
}

void DEditSubject::addSubject() {
    ui->twSubjects->disconnect();

    STSubject *add = new STSubject;
    add->id = INVALID_ID;

    addSubjectInTable(add);

    connect(ui->twSubjects, SIGNAL(itemChanged(QTableWidgetItem *)),
            SLOT(editSubject(QTableWidgetItem *)));
}

void DEditSubject::editSubject(QTableWidgetItem *inItem) {
    if (inItem->column() == 0) {
        return;
    }

    QVariant var = inItem->data(Qt::UserRole);
    QString &str = *var.value<QString *>();

    var = ui->twSubjects->item(inItem->row(), 0)->data(Qt::UserRole);
    STSubject *edit = var.value<STSubject *>();

    switch (inItem->column()) {
        // nick
        case 1: {
            if (inItem->text().length() > 22) {
                inItem->setText(inItem->text().mid(0, 22));
            }
            str = inItem->text();
            break;
        }
            // name
        case 2: {
            if (inItem->text() == "") {
                inItem->setText(str);
            } else {
                str = inItem->text();
            }
            break;
        }
            // other fields
        default: { str = inItem->text(); }
    }

    if (edit->nick != "") {
        if (edit->id == INVALID_ID) {
            edit->id = STDataProvider::getInstance().addSubject(*edit);
        } else {
            STDataProvider::getInstance().modSubject(*edit);
        }
    }
}

void DEditSubject::delSubject() {
    ui->twSubjects->disconnect();

    STSubject *del = NULL;
    for (int i = 0; i < ui->twSubjects->rowCount(); i++) {
        if (sender() == ui->twSubjects->cellWidget(i, 0)) {
            QVariant var;
            var = ui->twSubjects->item(i, 0)->data(Qt::UserRole);
            del = var.value<STSubject *>();
            ui->twSubjects->removeRow(i);
            break;
        }
    }
    if (del == NULL) {
        qDebug() << "Error on delete subject";
        return;
    }

    STDataProvider::getInstance().delSubject(del->id);
    delete del;

    connect(ui->twSubjects, SIGNAL(itemChanged(QTableWidgetItem *)),
            SLOT(editSubject(QTableWidgetItem *)));
}

void DEditSubject::showDelete(bool inShow) {
    if (inShow) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this, QString::fromUtf8("Внимание"),
                                     QString::fromUtf8(
                                         "Удаление предмета. Продолжить?"),
                                     QMessageBox::Yes | QMessageBox::No);
        if (reply != QMessageBox::Yes) {
            ui->pbDelRightLight->blockSignals(true);
            ui->pbDelRightLight->setChecked(false);
            ui->pbDelRightLight->blockSignals(false);
            return;
        }
        ui->twSubjects->showColumn(0);
    } else {
        ui->twSubjects->hideColumn(0);
    }
}

