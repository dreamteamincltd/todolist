#ifndef MAINWIN_H
#define MAINWIN_H

#include "st.include.h"
#include "calendar.h"
#include "edittask.h"
#include "settings.h"
#include "prog.h"

namespace Ui {
    class WStodo;
}

class WStodo : public QMainWindow {
        Q_OBJECT

    public:
        explicit WStodo(QWidget *parent = 0);
        ~WStodo();
        void enableSmallScreenSupport();

    protected:
        void showEvent(QShowEvent *inSE);
        void closeEvent(QCloseEvent *inCE);

    private:
        Ui::WStodo *ui;
        WSettingSchedule *settingSchedule;
        QDate selectedDate;
        int currentState;

    private slots:
  //      void refreshProg(int points);
        void showSettingSchedule();
        void changeSelectedDate(QDate inDate);

        void updateUiOnSelectedDate();
        void updateTasksUi();
        void updateTasks();
        void updateFilters();

        void slotIncWeek();
        void slotDecWeek();
        void slotHome();
        void slotIncMonth();
        void slotDecMonth();

        void switchStateToDialog(int inId);
        void switchStateToSchedule();
        void finishSwitch();

        void importDatabase();
        void exportDatabase();
     //   void exportICalSchedule();
        void exportICalTasks();
        void dbOnError();

        void openVk();
};
#endif // MAINWIN_H
