#ifndef TASKMOD_H
#define TASKMOD_H

#include <QString>
#include <QDate>

class STTask {
    public:
        enum Priority {
            LOW,
            NORMAL,
            HIGH,
            ALARM
        };

        int id;

        int subjectId;
        int seriesNumber;

        int points;
        bool isComplete;
        QDate date;
        QString task;
        QString description;

        int prior;

        bool isOutdated();
};

#endif // TASKMOD_H
