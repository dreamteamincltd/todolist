#include "taskMod.h"

bool STTask::isOutdated() {
    if (!isComplete) {
        return QDate::currentDate() > date;
    } else {
        return false;
    }
}
