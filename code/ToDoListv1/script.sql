PRAGMA foreign_keys = ON;
CREATE TABLE IF NOT EXISTS STGlobal
(
    num INTEGER Primary Key,
    data INTEGER
);


CREATE TABLE IF NOT EXISTS STTask
(
    id INTEGER Primary Key,
    dateD INTEGER,
    dateM INTEGER,
    dateY INTEGER,
    task TEXT,
    prior INTEGER,
    foreign Key (subjectId) references STSubject (id) on delete cascade on update cascade
);
