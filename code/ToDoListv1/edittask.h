#ifndef EDITTASK_H
#define EDITTASK_H
#define W_SLIDE_ANIMATION_PATH 2000
#include "st.include.h"
enum {
    REPEAT_DAY = 1,
    REPEAT_WEEK = 7,
    REPEAT_MONTH = 30
};

namespace Ui {
    class WTaskEdit;
}

class WTaskEdit : public QWidget {
        Q_OBJECT

    signals:
        void sigNeedBar();
        void sigNeedRefresh();

    public:
        explicit WTaskEdit(QWidget *parent = 0);
        ~WTaskEdit();
        void fillFields(int inId);
        void setDate(QDate inDate);

    public slots:
        void slideToHide();
        void slideToShow();

    private:
        Ui::WTaskEdit *ui;
        int currentId;
        int currentSeries;
        // void showEvent(QShowEvent *inSE);
        void resizeEvent(QResizeEvent *inRE);
        void clearFields();

    private slots:
        void toggleRepeat(bool inRepeat);
        void repeatPeriodChanged(int inNum);
        void saveAll();
        void saveTodo(QString inTitle, QDate inDate, int inSeries = -1);
        void addTodo();
        void removeTodo();
        void removeSeries();
        void closeEditor();
        void finishEdit();
        void checkDates(QDate inDate);
        void somethingChanged();
};
#endif // EDITTASK_H
