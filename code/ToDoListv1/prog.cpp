#include "prog.h"
#include "getdata.h"
#include "tasksonweek.h"
#include "ui_prog.h"
#include <iostream>

ProgressBar::ProgressBar(QWidget *parent):QFrame(parent), ui(new Ui::ProgressBar)
{
    ui->setupUi(this);
    ui->Cage->setObjectName("lbBooksIcon");

   // connect(&endResizeTimer, SIGNAL(timeout()), SLOT(computeNewWidth()));


}

ProgressBar::~ProgressBar() { delete ui; }

void ProgressBar::resizeEvent(QResizeEvent *inRE){
    if(endResizeTimer.isActive())
        endResizeTimer.stop();
    endResizeTimer.start(100);
    QRect buffRect = this->geometry();
    ui->group->setGeometry(buffRect);
}
void ProgressBar::fillBar(){

    float proc = STDataProvider::getInstance().getOutdatedTasks()->size();
    float a = STDataProvider::getInstance().getAllTasks()->size();
    std::cout << a << std::endl;
    std::cout <<proc << std::endl;
    proc = proc/a * 100;

    ui->Bar->setValue(proc);

}

void ProgressBar::slideToHide(){
    QPropertyAnimation *moveOutBar;
    moveOutBar = new QPropertyAnimation(ui->group, "geometry");
    moveOutBar->setDuration(W_SLIDE_ANIMATION_TIME);
    moveOutBar->setStartValue(ui->group->geometry());
    moveOutBar->setEndValue(QRect(ui->group->x() + 2000, ui->group->y(),
                                  ui->group->width(), ui->group->height()));
    moveOutBar->start();
    fillBar();
}
void ProgressBar::slideToShow(){
    QPropertyAnimation *moveBar;
        moveBar = new QPropertyAnimation(ui->group, "geometry");
        moveBar->setDuration(W_SLIDE_ANIMATION_TIME);
        moveBar->setStartValue(
                    QRect(2000, ui->group->y(),
                          ui->group->width(), ui->group->height()));
        moveBar->setEndValue(
                    QRect(0, 0, ui->group->width(), ui->group->height()));
        moveBar->start();
        fillBar();
}


