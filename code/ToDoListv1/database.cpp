#include "getdata.h"
#include "taskMod.h"

#include <QtCore>
#include <QMessageBox>

STICalExporter::STICalExporter(STDataProvider *inProvider, QObject *inParent)
    : QObject(inParent), provider(inProvider) {
    qsrand(QDateTime::currentMSecsSinceEpoch());
}

void STICalExporter::setProvider(STDataProvider *inValue) {
    provider = inValue;
}

void STICalExporter::exportTasks(QString inFileName) {
    if (provider == NULL) {
        qDebug() << "Provider is NULL";
        return;
    }

    QFile file(inFileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text |
                   QIODevice::Truncate)) {
        qDebug() << "Can't open file for writing";
        return;
    }

    QTextStream out(&file);
    out.setCodec("UTF-8");

    out << "BEGIN:VCALENDAR" << endl;
    out << "PRODID:-//CageDO//CageDO Tasks 1.0" << endl;
    out << "VERSION:2.0" << endl;
    out << "CALSCALE:GREGORIAN" << endl;
    out << "METHOD:PUBLISH" << endl;
    out << "X-WR-CALNAME:CageDO tasks" << endl;

    QVector<STTask *> *tasks = provider->getAllTasks();
    int uid = 0;

    STTask *task = NULL;
    STSubject *subject = NULL;
    for (int i = 0; i < tasks->size(); ++i) {
        task = tasks->at(i);

        if (!task) {
            qDebug() << "Can't get task from provider";
            return;
        }

        // Check complete
        if (task->isComplete) {
            continue;
        }

        if (task->subjectId != -1) {//INVALID_ID
            subject = provider->getSubject(task->subjectId);
        } else {
            subject = NULL;
        }

        QString format = "yyyyMMddThhmmss";
        QDateTime dateTime(task->date);
        dateTime = dateTime.toUTC();

        out << "BEGIN:VTODO" << endl;
        out << "UID:"
            << "STTask_" << uid << "@cagedo.ru" << endl;
        out << "DTSTART:" << dateTime.toString(format) << "Z" << endl;
        out << "DUE:" << dateTime.toString(format) << "Z" << endl;
        dateTime = QDateTime::currentDateTimeUtc();
        out << "DTSTAMP:" << dateTime.toString(format) << "Z" << endl;

        out << "SUMMARY:";
        out << task->task << ": ";

        if (subject) {
            if (!subject->name.isEmpty() && !subject->name.isNull()) {
                out << subject->name << endl;
            } else {
                out << subject->nick << endl;
            }
        } else {
            out << tr("Общее") << endl;
        }

        out << "CLASS:CONFIDENTIAL" << endl;
        out << "CATEGORIES:EDUCATION" << endl;

        out << "PRIORITY:";
        switch (task->prior) {
            case STTask::LOW: {
                out << 8;
                break;
            }
            case STTask::NORMAL: {
                out << 5;
                break;
            }
            case STTask::HIGH: {
                out << 3;
                break;
            }
            case STTask::ALARM: {
                out << 1;
                break;
            }
            default: {
                qDebug() << "Unknow prior";
                out << 0;
                break;
            }
        }
        out << endl;

        if (task->isComplete) {
            out << "STATUS:COMPLETED" << endl;
        } else {
            out << "STATUS:NEEDS-ACTION" << endl;
        }

        out << "END:VTODO" << endl;

        if (subject) {
            delete subject;
        }

        uid++;
    }

    clearQVector<STTask>(*tasks);
    delete tasks;

    out << "END:VCALENDAR" << endl;
}
