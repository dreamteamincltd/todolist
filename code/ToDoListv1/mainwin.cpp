#include "mainwin.h"
#include "ui_stodo.h"
#include <stdio.h>
#include <iostream>



WStodo::WStodo(QWidget *parent) : QMainWindow(parent), ui(new Ui::WStodo) {
    ui->setupUi(this);

    settingSchedule = new WSettingSchedule();
    changeSelectedDate(QDate::currentDate());

    ui->actQuit->setMenuRole(QAction::QuitRole);

    connect(ui->actQuit, SIGNAL(triggered()), SLOT(close()));

    connect(ui->actSettingSchedule, SIGNAL(triggered()), settingSchedule,
            SLOT(show()));

    connect(ui->actLoad, SIGNAL(triggered()), SLOT(importDatabase()));

    connect(ui->actSave, SIGNAL(triggered()), SLOT(exportDatabase()));

    connect(ui->actICalTasks, SIGNAL(triggered()), SLOT(exportICalTasks()));

    connect(ui->actionVk, SIGNAL(triggered()), SLOT(openVk()));

    connect(ui->actQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    // Update schedule
    connect(settingSchedule, SIGNAL(scheduleChanged()), SLOT(updateSheduleUi()));
    connect(&STDataProvider::getInstance(), SIGNAL(subjectsChanged()),
            SLOT(updateTasksUi()));

    //From ProgressBar
 //   connect(ui->wProgressBar,SIGNAL(RefreshProg(int)), SLOT(refreshProg(int)));
    // From WCalendar
    connect(ui->wCalendar, SIGNAL(selectedDateChanged(QDate)),
            SLOT(changeSelectedDate(QDate)));
    connect(ui->wCalendar, SIGNAL(sigIncMonth()), SLOT(slotIncMonth()));
    connect(ui->wCalendar, SIGNAL(sigDecMonth()), SLOT(slotDecMonth()));

    // From WTasks
    connect(ui->wTasks, SIGNAL(sigNeedDialog(int)),
            SLOT(switchStateToDialog(int)));

    // From TaskEdit
    connect(ui->wTaskEdit, SIGNAL(sigNeedBar()),
            SLOT(switchStateToSchedule()));
    connect(ui->wTaskEdit, SIGNAL(sigNeedRefresh()), SLOT(updateTasks()));

    // From WFilter
    connect(ui->wFilter, SIGNAL(filtersChanged()), SLOT(updateFilters()));

    show();
    }
WStodo::~WStodo() {
    delete settingSchedule;
    delete ui;
}

void WStodo::enableSmallScreenSupport() {
    QRect buffRect = this->geometry();
    this->setGeometry(buffRect.x(), buffRect.y(), 1024, 575);
    settingSchedule->enableSmallScreenSupport();
    settingSchedule->setGeometry(buffRect.x() + 24, buffRect.y() + 24, 524, 320);
    settingSchedule->setMinimumSize(524, 320);
}

void WStodo::showEvent(QShowEvent *inSE) { inSE->accept(); }

void WStodo::closeEvent(QCloseEvent *inCE) {
    settingSchedule->close();
    inCE->accept();
}

void WStodo::showSettingSchedule() {
    settingSchedule->show();
    settingSchedule->activateWindow();
    settingSchedule->raise();
}

void WStodo::changeSelectedDate(QDate inDate)
{
    selectedDate = inDate;

    ui->wCalendar->fillCalendarOnDate(selectedDate);
    ui->wTasks->fillTasksOnDate(selectedDate);
    ui->wTaskEdit->setDate(selectedDate);
}

void WStodo::updateUiOnSelectedDate() { changeSelectedDate(selectedDate); }

void WStodo::updateTasksUi() {
    ui->wTasks->fillTasksOnDate(selectedDate);
    ui->wProgressBar->fillBar();
    ui->wTaskEdit->setDate(selectedDate);
}
void WStodo::updateTasks() { ui->wTasks->refreshAll(); }

void WStodo::updateFilters() {
    ui->wTasks->updateFilter(ui->wFilter->currentFilter);
}

void WStodo::slotIncWeek() { changeSelectedDate(selectedDate.addDays(7)); }

void WStodo::slotDecWeek() { changeSelectedDate(selectedDate.addDays(-7)); }

void WStodo::slotHome() { changeSelectedDate(QDate::currentDate()); }

void WStodo::slotIncMonth() { changeSelectedDate(selectedDate.addMonths(1)); }

void WStodo::slotDecMonth() { changeSelectedDate(selectedDate.addMonths(-1)); }

void WStodo::switchStateToDialog(int inId) {
    ui->wTaskEdit->fillFields(inId);

    if (currentState != 1) {
        ui->wProgressBar->slideToHide();
        currentState = 1;
        QTimer::singleShot(W_SLIDE_ANIMATION_TIME, this, SLOT(finishSwitch()));
    }
}

void WStodo::switchStateToSchedule() {
    ui->wTaskEdit->slideToHide();
    currentState = 0;
    QTimer::singleShot(W_SLIDE_ANIMATION_TIME, this, SLOT(finishSwitch()));

    ui->wTasks->refreshAll();
}

void WStodo::finishSwitch() {
    if (currentState == 1) {
        ui->stackedWidget->setCurrentIndex(1);
        ui->wTaskEdit->slideToShow();
    } else {
        std::cout << currentState<< std::endl;
        ui->stackedWidget->setCurrentIndex(0);
        ui->wProgressBar->slideToShow();
    }
}

void WStodo::importDatabase() {
    QString fileName = QFileDialog::getOpenFileName(
                this, QString::fromUtf8("Загрузка данных"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                QString::fromUtf8("CageDO format (*.std)"));

    STDataProvider::getInstance().importDatabase(fileName);

    updateUiOnSelectedDate();
    updateFilters();
}

void WStodo::exportDatabase() {
    QString fileName = QFileDialog::getSaveFileName(
                this, QString::fromUtf8("Сохранение данных"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                QString::fromUtf8("CageDO format (*.std)"));

    if (fileName.isEmpty() || fileName.isNull()) {
        return;
    }

    if (!QFile::copy(STDataProvider::getInstance().getDatabaseName(), fileName)) {
        QMessageBox::warning(
                    this, QString::fromUtf8("Ошибка"),
                    QString::fromUtf8("Не удалось скопировать файл с данными"));
    }
}

void WStodo::exportICalTasks() {
    QString fileName = QFileDialog::getSaveFileName(
                this, QString::fromUtf8("Экспорт задач"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                QString::fromUtf8("iCal file format (*.ics)"));

    if (fileName.isEmpty() || fileName.isNull()) {
        return;
    }

    STICalExporter exporter(&STDataProvider::getInstance());
    exporter.exportTasks(fileName);
}
void WStodo::dbOnError()
{
    QMessageBox::warning(NULL,
                         "Db",
                         "Возникла ошибка, попробуйте повторить операцию еще раз");
}


void WStodo::openVk() {
    QDesktopServices::openUrl(QUrl("http://vk.com/yourfavourite_martian"));
}

