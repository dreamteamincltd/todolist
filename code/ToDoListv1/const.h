#ifndef CONST_H
#define CONST_H
#include <QtGlobal>
#include <QString>

#define ST_OPENSOURCE true
#define ST_BASIC_VERSION 1300

#ifdef Q_OS_WIN32
#define ST_VERSION ST_BASIC_VERSION+1
#endif

#ifdef Q_OS_LINUX
#define ST_VERSION ST_BASIC_VERSION+2
#endif

#ifdef Q_OS_MAC
#define ST_VERSION ST_BASIC_VERSION+3
#endif

#define ST_STR_VERSION QString("%1.%2.%3")\
    .arg((ST_VERSION)/1000%10)\
    .arg((ST_VERSION)/100%10)\
    .arg((ST_VERSION)/10%10)

#define DB_NAME "CageDO.db"
#define DP_VERSION 1
#define INVALID_ID -1
#define EMPTY_COLOR 244,247,252

#define SET_DELETE_ITEM "containers/delete_item"
#define SET_DELETE_BELL "editschedule/delete_bell"
#define SET_TOKEN_SECRET "token/secret"
#define SET_TOKEN "token/token"

#define FIRST_DAY_OF_WEEK Qt::Monday
#define LAST_DAY_OF_WEEK Qt::Sunday

#define W_SLIDE_ANIMATION_TIME 300

#endif // CONST_H
