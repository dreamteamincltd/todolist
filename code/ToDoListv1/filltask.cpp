#include "filltask.h"
#include "ui_filltask.h"

WTaskLine::WTaskLine(QWidget *parent) : QWidget(parent), ui(new Ui::WTaskLine) {
    ui->setupUi(this);
    connect(ui->cbTaskCheck, SIGNAL(toggled(bool)), SLOT(stateChanged(bool)));
}

WTaskLine::~WTaskLine() {
    delete ui;
    delete currTask;
}

void WTaskLine::fillTask(STTask *inTask) {
    currTask = inTask;
    id = inTask->id;

    ui->lbTaskTitle->setText(inTask->task);

    // Fill subject of line, TODO: add subj color
    if (inTask->subjectId != -1) {
        QString buffStr =
                STDataProvider::getInstance().getSubject(inTask->subjectId)->nick;
        //        if( buffStr.length() > 12 ) {
        //            buffStr = buffStr.mid(0, 12);
        //        }
        ui->lbTaskSubj->setText(buffStr);
    } else {
        ui->lbTaskSubj->setText(" Общее ");
    }

    ui->cbTaskCheck->setChecked(currTask->isComplete);

    switch (currTask->prior) {
        case 0:
            ui->lbPriority->setObjectName("lbPriorityLow");
        break;
        case 1:
            ui->lbPriority->setObjectName("lbPriorityNorm");
        break;
        case 2:
            ui->lbPriority->setObjectName("lbPriorityHigh");
        break;
        case 3:
            ui->lbPriority->setObjectName("lbPriorityAlarm");
        break;
    }
}

void WTaskLine::mousePressEvent(QMouseEvent *ev) {
    if (ev->button() == Qt::LeftButton) {
        ev->accept();
        emit clicked(id);
    } else {
        ev->ignore();
    }
}

void WTaskLine::stateChanged(bool inState) {
    currTask->isComplete = inState;
    STDataProvider::getInstance().modTask(*currTask);
}
