#ifndef EDITSUB_H
#define EDITSUB_H

#include "st.include.h"
#include "sql.h"

namespace Ui {
    class DEditSubject;
}

class DEditSubject : public QDialog {
        Q_OBJECT

    public:
        explicit DEditSubject(QWidget *parent = 0);
        ~DEditSubject();

    protected:
        void showEvent(QShowEvent *inSE);
        void closeEvent(QCloseEvent *inCE);
        virtual bool eventFilter(QObject *inObject, QEvent *inEvent);

    private:
        Ui::DEditSubject *ui;

        bool isHaveFakeSubjects();
        void clearTable();
        void addSubjectInTable(STSubject *inSubject);
        void fillTable();

        void disableTableEditMode();

    private slots:
        void addSubject();
        void editSubject(QTableWidgetItem *inItem);
        void delSubject();
        void showDelete(bool inShow);
};

#endif // EDITSUB_H
