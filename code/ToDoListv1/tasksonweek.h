#ifndef TASKSONWEEK_H
#define TASKSONWEEK_H

#include "st.include.h"
#include "taskcompleted.h"

namespace Ui {
    class WTasksOnWeek;
}

class WTasksOnWeek : public QWidget {
        Q_OBJECT

    signals:
        void sigNeedDialog(int outId);

    public:
        explicit WTasksOnWeek(QWidget *parent = 0);
        ~WTasksOnWeek();

    public slots:
        void fillTasksOnDate(QDate inDate);
        void refreshAll();
        void updateFilter(STFilterElement inFilter);

    private:
        Ui::WTasksOnWeek *ui;

        int currentState;
        QDate currentDate;
        WDaysTasks *days[7];
        WTasksStub stub;
        STFilterElement currentFilter;

    private slots:
        void addTask();
        void editTask(int inId);

        void showCompleted(bool inState);
        void showOverDue(bool inState);
        void showAll(bool inState);
};


#endif // TASKSONWEEK_H
