#ifndef SQL_H
#define SQL_H
#define DP_CREATE_GLOBAL "\
CREATE TABLE IF NOT EXISTS STGlobal  \
( \
    num INTEGER Primary Key, \
    data INTEGER \
);"


#define DP_CREATE_SUBJECT "\
CREATE TABLE IF NOT EXISTS STSubject \
( \
    id INTEGER Primary Key, \
    name TEXT, \
    nick TEXT, \
    check( nick is NOT NULL and nick<>\'\' ) \
);"

#define DP_CREATE_TASK "\
CREATE TABLE IF NOT EXISTS STTask  \
( \
    id INTEGER Primary Key, \
    subjectId INTEGER, \
    seriesNumber INTEGER, \
    isComplete BOOLEAN, \
    dateD INTEGER, \
    dateM INTEGER, \
    dateY INTEGER, \
    task TEXT, \
    description TEXT, \
    prior INTEGER, \
    check( task is NOT NULL and task<>\'\' ), \
    foreign Key (subjectId) references STSubject (id) on delete cascade on update cascade \
);"



#endif // SQL_H
