#ifndef GETDATA_H
#define GETDATA_H
#include <QObject>
#include "database.h"

class STICalExporter : public QObject {
        Q_OBJECT

    public:
        explicit STICalExporter(STDataProvider *inProvider = 0,
                                QObject *inParent = 0);
        void setProvider(STDataProvider *inValue);
        void exportTasks(QString inFileName);

    private:
        STDataProvider *provider;
};

#endif // GETDATA_H
