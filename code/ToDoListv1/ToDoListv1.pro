TEMPLATE = app

QT += qml quick widgets network sql
QTPLUGIN += qsqlite

win32:RC_FILE = iStodo.rc

SOURCES += main.cpp \
    task.cpp \
    database.cpp \
    getdata.cpp \
    alert.cpp \
    editsub.cpp \
    edittask.cpp \
    filltask.cpp \
    taskcompleted.cpp \
    daytask.cpp \
    calendar.cpp \
    filterelement.cpp \
    filter.cpp \
    mainwin.cpp \
    tasksonweek.cpp \
    exportfile.cpp \
    settings.cpp \
    container.cpp \
    prog.cpp

RESOURCES += res.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += file.h \
    taskMod.h \
    sql.h \
    getdata.h \
    st.include.h \
    database.h \
    type.h \
    editsub.h \
    edittask.h \
    filltask.h \
    taskcompleted.h \
    daytask.h \
    calendar.h \
    filterelement.h \
    filter.h \
    const.h \
    mainwin.h \
    tasksonweek.h \
    exportfile.h \
    settings.h \
    container.h \
    prog.h

OTHER_FILES += \
    script.sql \
    buttons.css \
    scrollbars.css \
    style.css \
    tables_frames.css

FORMS += \
    w.container.ui \
    calendar.ui \
    daytask.ui \
    editsub.ui \
    edittask.ui \
    filltask.ui \
    filter.ui \
    taskcompleted.ui \
    tasksonweek.ui \
    w.settingschedule.ui \
    stodo.ui \
    prog.ui
