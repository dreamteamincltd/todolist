#include "settings.h"
#include "ui_w.settingschedule.h"

WSettingSchedule::WSettingSchedule(QWidget *parent)
    : QWidget(parent), ui(new Ui::WSettingSchedule) {
    ui->setupUi(this);

    editSubject = new DEditSubject(this);

    //  Containers
    ui->cSubjects->setType(CONTAINER_SUBJECTS);

    connect(&STDataProvider::getInstance(), SIGNAL(subjectsChanged()),
            SLOT(fillSubjectList()));
    connect(&STDataProvider::getInstance(), SIGNAL(subjectsChanged()),
            SLOT(sendScheduleChanged()));

    connect(ui->cSubjects->pbSettings, SIGNAL(clicked()), editSubject,
            SLOT(show()));

    // drag element
    connect(ui->cSubjects->lwList, SIGNAL(currentRowChanged(int)),
            SLOT(setDragSubject(int)));

}

WSettingSchedule::~WSettingSchedule() {
    clearSubjectList();

    delete ui;

    delete editSubject;
}
void WSettingSchedule::enableSmallScreenSupport() {
    ui->cSubjects->setFixedHeight(222);
    ui->groupFrame->setFixedWidth(370);
    ui->permanentTip->hide();
}

void WSettingSchedule::showEvent(QShowEvent *inSE) {
    fillSubjectList();

    inSE->accept();
}

void WSettingSchedule::addSubjectInList(STSubject *inSubject) {
    QListWidgetItem *item = new QListWidgetItem();
    item->setText(inSubject->nick);

    QVariant var;
    var.setValue(inSubject);
    item->setData(Qt::UserRole, var);

    ui->cSubjects->lwList->addItem(item);
}

void WSettingSchedule::clearSubjectList() {
    QVariant var;
    STSubject *del = NULL;
    while (ui->cSubjects->lwList->count() > 0) {
        var = ui->cSubjects->lwList->item(0)->data(Qt::UserRole);
        del = var.value<STSubject *>();
        delete del;

        delete ui->cSubjects->lwList->takeItem(0);
    }
    ui->cSubjects->lwList->clear();
}

void WSettingSchedule::fillSubjectList() {
    clearSubjectList();

    QVector<STSubject *> *subjects = STDataProvider::getInstance().getSubjects();
    for (int i = 0; i < subjects->size(); i++) {
        addSubjectInList((*subjects)[i]);
    }
    // Change buttons for clearity
    if (subjects->size()) {
        ui->cSubjects->setState(true);
    } else {
        ui->cSubjects->setState(false);
    }

    delete subjects;
}

void WSettingSchedule::sendScheduleChanged() { emit scheduleChanged(); }

void WSettingSchedule::helpRequested() { emit needHelp(); }
