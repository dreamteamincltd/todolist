#ifndef FILLTASK_H
#define FILLTASK_H

#include "st.include.h"

namespace Ui {
    class WTaskLine;
}

class WTaskLine : public QWidget {
        Q_OBJECT
    signals:
        void clicked(int inId);

    public:
        explicit WTaskLine(QWidget *parent = 0);
        ~WTaskLine();

        void fillTask(STTask *inTask);

        int id;

    private:
        void mousePressEvent(QMouseEvent *ev);

        STTask *currTask;
        Ui::WTaskLine *ui;

    private slots:
        void stateChanged(bool inState);
};

#endif // FILLTASK_H
