#include "container.h"
#include "ui_w.container.h"

WContainer::WContainer(QWidget *parent)
    : QWidget(parent), ui(new Ui::WContainer) {
    ui->setupUi(this);

    lblHeader = ui->lblHeader;
    pbSettings = ui->pbSettings;
    lwList = ui->lwList;

}

WContainer::~WContainer() { delete ui; }

void WContainer::setType(int inType) {
    switch (inType) {
        case CONTAINER_SUBJECTS: {
            ui->lblHeader->setText("Типы заданий");
            ui->lbStubText->setText("добавьте типы заданий");
            ui->lbStubIcon->setObjectName("lbBooksIcon");
            ui->lbStubIcon->setFixedSize(100, 128);
            break;
        }
    }
    ui->lbStubIcon->setStyle(NULL);
}

void WContainer::setState(bool isEdit) {
    if (isEdit) {
        pbSettings->setObjectName("pbExpandSingleBlue");
        ui->stackedWidget->setCurrentIndex(PAGE_LIST);
    } else {
        pbSettings->setObjectName("pbAddSingleBlue");
        ui->stackedWidget->setCurrentIndex(PAGE_STUB);
    }
    pbSettings->setStyle(NULL);
}
