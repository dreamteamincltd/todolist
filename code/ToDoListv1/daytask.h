   #ifndef DAYTASK_H
#define DAYTASK_H

#include "st.include.h"
#include "filltask.h"

enum {
    STATE_CALENDAR,
    STATE_OVERDUE,
    STATE_ALL
};

namespace Ui {
    class WDaysTasks;
}

class WDaysTasks : public QWidget {
        Q_OBJECT

    signals:
        void sigNeedTaskEdit(int);

    public:
        explicit WDaysTasks(QWidget *parent = 0);
        ~WDaysTasks();
        bool fillTasks(QDate inDate, STFilterElement inFilter);
        bool fillOverDue();
        bool fillAllTasks(STFilterElement inFilter);

        int id;

    private:
        Ui::WDaysTasks *ui;

        int currentState;
        QDate currentDate;
        STFilterElement currentFilter;

    private slots:
        void taskSelected(int inId);
        bool refreshAll();
};

#endif // DAYTASK_H
