#include "tasksonweek.h"
#include "ui_tasksonweek.h"

WTasksOnWeek::WTasksOnWeek(QWidget *parent)
    : QWidget(parent), ui(new Ui::WTasksOnWeek) {
    ui->setupUi(this);

    // shortcut block
    QShortcut *newShortcut =
            new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_N), this);
#ifdef Q_OS_MAC
    ui->pbAddSingleDark->setToolTip("Добавить новую задачу (⌘+N)");
#else
    ui->pbAddSingleDark->setToolTip("Добавить новую задачу (Ctrl+N)");
#endif
    connect(newShortcut, SIGNAL(activated()), SLOT(addTask()));

    // buttons connection
    connect(ui->pbAddSingleDark, SIGNAL(clicked()), SLOT(addTask()));

    connect(ui->pbDoneSingleDark, SIGNAL(toggled(bool)),
            SLOT(showCompleted(bool)));

    connect(ui->pbShowAll, SIGNAL(toggled(bool)), SLOT(showAll(bool)));

    connect(ui->pbOverDue, SIGNAL(toggled(bool)), SLOT(showOverDue(bool)));

    currentFilter.showCompleted = false;

    currentState = STATE_CALENDAR;

    for (int i = 0; i < 7; i++) {
        days[i] = new WDaysTasks();
        days[i]->id = i;

        connect(days[i], SIGNAL(sigNeedTaskEdit(int)), SLOT(editTask(int)));

        ui->vbDaysBox->addWidget(days[i]);
    }

    ui->vbDaysBox->addStretch();
    ui->vbDaysBox->addWidget(&stub);
    ui->vbDaysBox->addStretch();
}

WTasksOnWeek::~WTasksOnWeek() {
    delete ui;

    for (int i = 0; i < 7; i++) {
        delete days[i];
    }
}

void WTasksOnWeek::fillTasksOnDate(QDate inDate) {
    QDate date = inDate;

    while (date.dayOfWeek() != FIRST_DAY_OF_WEEK) {
        date = date.addDays(-1);
    }
    currentDate = date;
    currentState = STATE_CALENDAR;
    ui->pbOverDue->setChecked(false);
    ui->pbShowAll->setChecked(false);
    refreshAll();
}

void WTasksOnWeek::refreshAll() {
    // configure stub
    bool needStub = true;
    stub.setState(currentState);

    switch (currentState) {
        case STATE_CALENDAR: {
            QDate date = currentDate;

            for (int i = 0;; i++) {
                if (!days[i]->fillTasks(date, currentFilter)) {
                    ui->vbDaysBox->itemAt(i)->widget()->setHidden(true);
                } else {
                    needStub = false;
                    ui->vbDaysBox->itemAt(i)->widget()->setHidden(false);
                }

                if (date.dayOfWeek() == LAST_DAY_OF_WEEK) {
                    break;
                }
                date = date.addDays(1);
            }
            break;
        }
        case STATE_OVERDUE: {
            if (!days[0]->fillOverDue()) {
                ui->vbDaysBox->itemAt(0)->widget()->setHidden(true);
            } else {
                needStub = false;
                ui->vbDaysBox->itemAt(0)->widget()->setHidden(false);
            }
            break;
        }
        case STATE_ALL: {
            if (!days[0]->fillAllTasks(currentFilter)) {
                ui->vbDaysBox->itemAt(0)->widget()->setHidden(true);
            } else {
                needStub = false;
                ui->vbDaysBox->itemAt(0)->widget()->setHidden(false);
            }
            break;
        }
    }
    ui->pbOverDue->setText(QString::number(
                               STDataProvider::getInstance().getOutdatedTasks()->size()));
    if (needStub) {
        stub.setHidden(false);
    } else {
        stub.setHidden(true);
    }
}

void WTasksOnWeek::updateFilter(STFilterElement inFilter) {
    bool buffDone = currentFilter.showCompleted;
    currentFilter = inFilter;
    currentFilter.showCompleted = buffDone;
    refreshAll();
}

void WTasksOnWeek::addTask() { emit sigNeedDialog(INVALID_ID); }

void WTasksOnWeek::editTask(int inId) { emit sigNeedDialog(inId); }

void WTasksOnWeek::showCompleted(bool inState) {
    currentFilter.showCompleted = inState;
    refreshAll();
}

//TODO: Rewrite to button group, now it's bike

void WTasksOnWeek::showOverDue(bool inState) {
    // if this button checked in
    if (inState) {
        // if another button pressed
        if (currentState == STATE_ALL) {
            currentState = STATE_OVERDUE;
            ui->pbShowAll->setChecked(false);
        } else {
            currentState = STATE_OVERDUE;
        }

        for (int i = 0; i < 7; i++) {
            days[i]->setHidden(true);
        }
    }  // if this button checked out
    else {
        // If another button not pressed, we must go back to calendar mode
        if (currentState != STATE_ALL) {
            currentState = STATE_CALENDAR;
        } else {  // else state already switched by another button
            return;
        }
    }

    refreshAll();
}

void WTasksOnWeek::showAll(bool inState) {
    if (inState) {
        if (currentState == STATE_OVERDUE) {
            currentState = STATE_ALL;
            ui->pbOverDue->setChecked(false);
        } else {
            currentState = STATE_ALL;
        }

        for (int i = 0; i < 7; i++) {
            days[i]->setHidden(true);
        }
    } else {
        if (currentState != STATE_OVERDUE) {
            currentState = STATE_CALENDAR;
        } else {
            return;
        }
    }

    refreshAll();
}
