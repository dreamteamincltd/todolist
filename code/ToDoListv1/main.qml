import QtQuick 2.2
import QtQuick.Controls 1.1
import FileIO 1.0
//import mytodo 1.0

ApplicationWindow {
    id: applicationWindow1
    visible: true
    width: 640
    height: 480
    title: qsTr("toDoListApp")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }
    ListModel{
        id:tasksModel
    }

    ListView {

        id: listPlanView
        anchors.fill: parent
        width: parent.width
        transformOrigin: Item.Center
        model: tasksModel
        delegate: listItem
        clip: true

        add: Transition { NumberAnimation { properties: "y"; from: root.height; duration: 250 } }
        removeDisplaced: Transition { NumberAnimation { properties: "y"; duration: 150 } }
        remove: Transition { NumberAnimation { property: "opacity"; to: 0; duration: 150 } }

        FileIO{
            id:listdos
            source:"data.txt"
            onError: console.log(msg)

        }
        BorderImage {
            id: footer

            width: parent.width
            anchors.bottom: parent.bottom
            source: "qrc:images/delegate.png"
            border.left: 5; border.top: 5
            border.right: 5; border.bottom: 5

            Rectangle {
                y: -1 ; height: 1
                width: parent.width
                color: "#bbb"
            }
            Rectangle {
                y: 0 ; height: 1
                width: parent.width
                color: "white"
            }


            BorderImage {

                anchors.left: parent.left
                anchors.right: addButton.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 16
                source:"images/textfield.png"
                border.left: 14 ; border.right: 14 ; border.top: 8 ; border.bottom: 8

                TextInput{
                    id: textInput
                    anchors.fill: parent
                    clip: true
                    anchors.leftMargin: 14
                    anchors.rightMargin: 14
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 22
                    Text {
                        id: placeholderText
                        anchors.fill: parent
                        verticalAlignment: Text.AlignVCenter
                        visible: !(parent.text.length || parent.inputMethodComposing)
                        font: parent.font
                        text: "New todo..."
                        color: "#aaa"
                    }
                    onAccepted: {

                        tasksModel.append({"title": textInput.text, "completed": false})
                        textInput.text = ""
                    }
                }
            }

            Item {
                id: addButton

                width: 40 ; height: 40
                anchors.margins: 20
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                enabled: textInput.text.length
                Image {
                    source: addMouseArea.pressed ? "images/add_icon_pressed.png" : "images/add_icon.png"
                    anchors.centerIn: parent
                    opacity: enabled ? 1 : 0.5
                }
                MouseArea {
                    id: addMouseArea
                    anchors.fill: parent
                    onClicked:textInput.accepted()

                }
            }
        }
        Component{
            id: listItem
            BorderImage{
                id: item
                width: parent.width
                height: 80
                source: mouse.pressed ? "qrc:images/delegate_pressed.png":"images/delegate.png"
                Image{
                    id: shadow
                    anchors.top: parent.bottom
                    width: parent.width
                    visible: !mouse.pressed
                    source: "images/shadow.png"
                }
                MouseArea{
                    id: mouse
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked:{
                        if(index !== -1 && _synced){
                            tasksModel.setProperty(index, "completed", !completed)

                        }
                    }
                }
                Image {
                    id: check
                    anchors.left: parent.left
                    anchors.leftMargin: 16
                    width: 32
                    fillMode: Image.PreserveAspectFit
                    anchors.verticalCenter: parent.verticalCenter
                    source: completed ? "images/done.png" : ""
                }
                Text {
                    id: missionText
                    text: task
                    font.pixelSize: 26
                    color: "#333"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: check.right
                    anchors.right: parent.right
                    anchors.leftMargin: 12
                    anchors.rightMargin: 40
                    elide: Text.ElideRight
                }
                Image{
                    id :deleteButton

                    source: removeMouseArea.pressed ? "images/delete_icon_pressed.png" : "images/delete_icon.png"
                    anchors.margins: 20
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    opacity: enabled ? 1 : 0.5
                    Behavior on opacity {NumberAnimation{duration: 100}}

                    MouseArea {
                        id: removeMouseArea
                        anchors.fill: parent
                        onClicked: Model.remove(index)
                    }
                }
            }

        }

    }
}




