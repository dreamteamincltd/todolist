#ifndef CONTAINER_H
#define CONTAINER_H

#include "st.include.h"

enum {
    CONTAINER_SUBJECTS
};

enum {
    PAGE_LIST,
    PAGE_STUB
};

namespace Ui {
    class WContainer;
}

class WContainer : public QWidget {
        Q_OBJECT

    public:
        explicit WContainer(QWidget *parent = 0);
        ~WContainer();

        void setType(int inType);
        void setState(bool isEdit);

        QLabel *lblHeader;
        QPushButton *pbSettings;
        QListWidget *lwList;

    private:
        Ui::WContainer *ui;
};

#endif // CONTAINER_H
