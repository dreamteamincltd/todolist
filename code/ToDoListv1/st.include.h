#ifndef ST_INCLUDE_H
#define ST_INCLUDE_H
#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QProgressBar>
#include <QtNetwork>
#include "getdata.h"
#include "filterelement.h"
#include "const.h"

Q_DECLARE_METATYPE(QString*)
Q_DECLARE_METATYPE(STSubject*)

class STUpdaterClient;
class WTaskEdit;
class DEditSubject;
class WCalendar;
class WContainer;
class WStodo;
class Bar;
class WPageShow;

#endif // ST_INCLUDE_H
