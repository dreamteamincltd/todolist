#ifndef DATABASE_H
#define DATABASE_H

#include <QtCore>
#include <QtSql>
#include <QMessageBox>
#include "taskMod.h"
#include "type.h"
#include "const.h"

template <typename T>
void clearQVector(QVector<T*>& inVector) {
    for (int i = 0; i < inVector.size(); i++) {
        delete inVector.at(i);
    }
    inVector.clear();
}

template <typename T>
QVector<T*>* deepCopyQVector(QVector<T*>& inVector) {
    QVector<T*>* ret = new QVector<T*>();
    ret->reserve(inVector.size());
    for (int i = 0; i < inVector.size(); i++) {
        ret->append(new T(*inVector.at(i)));
    }
    return ret;
}

class STDataProvider : public QObject {
        Q_OBJECT

    signals:
        void globalChanged();
        void subjectsChanged();
        void tasksChanged();
        void updated();

    public:
        STDataProvider(QString inName = QString());
        ~STDataProvider();

        const static char* stringDayOfWeek[];

        static STDataProvider& getInstance();

        //  Check
        bool isEmpty();
        QString getDatabaseName();
        int getVersion();

        QDate getBegin();
        void setBegin(QDate inDate);
        QDate getEnd();
        void setEnd(QDate inDate);

        int getCountOfWeek();
        QDate getDateFromWeek(int inWeek, int inDayOfWeek);
        int getWeekFromDate(QDate inDate);
        QVector<STSubject*>* getSubjects();
        STSubject* getSubject(int inId);
        int addSubject(STSubject inSubject);
        void modSubject(STSubject inSubject);
        void delSubject(STSubject inSubject);
        void delSubject(int inId);

        QVector<STTask*>* getTaskOnDate(QDate inDate);
        QVector<QVector<STTask*>*>* getTaskOnWeekNumber(int inWeekNumber);
        QVector<STTask*>* getAllTasks();
        QVector<STTask*>* getOutdatedTasks();
        STTask* getTask(int inId);
        int addTask(STTask inTask);
        void modTask(STTask inTask);
        void delTask(STTask inTask);
        void delTask(int inId);

        int getNewSeriesNumber();
        void delTaskSeries(int inSeries);

        void importDatabase(QString inFileName);

    private:
        enum STGlobal {
            version,  // Database version
            dBegin,
            mBegin,
            yBegin,
            dEnd,
            mEnd,
            yEnd
        };

        QVector<STSubject*>* subjects;
        QVector<STTask*>* tasks;

        void initFunc();
        void fillFunc();

        int getIdFromVariant(QVariant inVar);
        QVariant getVariantFromId(int inId);

        template <class T>
        int getIndexById(QVector<T*>& inVector, int inId) {
            for (int i = 0; i < inVector.size(); i++) {
                if (inVector.at(i)->id == inId) {
                    return i;
                }
            }
            qFatal("Can't find index by id");
            return -1;
        }
};



#endif // DATABASE_H
