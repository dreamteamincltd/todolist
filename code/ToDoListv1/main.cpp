#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QGuiApplication>
#include<QObject>
#include <QString>
#include <QQmlEngine>
#include <QTextStream>
#include <QWidget>
#include <QQmlContext>
#include <QQuickView>
#include <QDir>
#include <QSettings>
#include "file.h"
#include <QFile>
#include <iostream>
#include <stdio.h>
#include <QtQml>
#include <fstream>
#include "getdata.h"
#include "st.include.h"
#include "mainwin.h"

FileIO::FileIO(QObject *parent) :
    QObject(parent)
{

}

QString FileIO::read()
{
    if (mSource.isEmpty()){
        emit error("source is empty");
        return QString();
    }

    QFile file(mSource);
    QString fileContent;
    if ( file.open(QIODevice::ReadOnly) ) {
        QString line;
        QTextStream t( &file );
        do {
            line = t.readLine();
            fileContent += line;
         } while (!line.isNull());

        file.close();
    } else {
        emit error("Unable to open the file");
        return QString();
    }
    return fileContent;
}

bool FileIO::write(const QString& data)
{
    if (mSource.isEmpty())
        return false;

    QFile file(mSource);
    if (!file.open(QFile::WriteOnly | QFile::Truncate))
        return false;

    QTextStream out(&file);
    out << data;

    file.close();

    return true;
}



int main(int argc, char *argv[])
{
    qmlRegisterType<FileIO, 1>("FileIO", 1, 0, "FileIO");
    //qmlRegisterType<STDataProvider>("mytodo", 1, 0, "STDataProvider");
    //qmlRegisterType<STGlobalTable>("mytodo", 1, 0, "STGlobalTable");
   // qmlRegisterType<STTask>("mytodo", 1, 0, "STTask");
   // qmlRegisterType<STSubject>("mytodo" , 1, 0 , "STSubject");

   // QGuiApplication app(argc, argv);
   // QStringList datalist;
    /*ifstream file("listdos.txt");
/*
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));
*/
    QApplication a(argc, argv);

    QFile styleF, buttonsF, tablesF, scrollsF;

    styleF.setFileName(":/qss/style.css");
    buttonsF.setFileName(":/qss/buttons.css");
    tablesF.setFileName(":/qss/tables_frames.css");
    scrollsF.setFileName(":/qss/scrollbars.css");

    styleF.open(QFile::ReadOnly);
    buttonsF.open(QFile::ReadOnly);
    tablesF.open(QFile::ReadOnly);
    scrollsF.open(QFile::ReadOnly);

    QString qssStr = styleF.readAll();
    qssStr += buttonsF.readAll();
    qssStr += tablesF.readAll();

    qApp->setStyleSheet(qssStr);


    WStodo w;
        if (QApplication::desktop()->width() <= 1024) {
            w.enableSmallScreenSupport();
        } else {
            w.setMinimumWidth(1150);
        }


   // view.setResizeMode(QQuickView::SizeRootObjectToView);
    //view.show();
    return a.exec();
}
