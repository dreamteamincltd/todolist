#ifndef FILTER_H
#define FILTER_H

#include "st.include.h"

namespace Ui {
    class WFilter;
}

class WFilter : public QWidget {
        Q_OBJECT

    signals:
        void filtersChanged();

    public:
        explicit WFilter(QWidget *parent = 0);
        ~WFilter();

        STFilterElement currentFilter;

    private:
        Ui::WFilter *ui;
        QVector<int> currentSubjects;

    private slots:
        void checkAll();
        void uncheckAll();
        void oneSubjChanged();
        void subjectsChanged();
};

#endif // FILTER_H
