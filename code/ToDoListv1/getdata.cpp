#include "database.h"
#include "st.include.h"
#include "sql.h"

#define DP_QUERY_ERROR \
    qFatal("%s", query.lastError().text().toStdString().c_str())

const char* STDataProvider::stringDayOfWeek[] = {"null", "Пн", "Вт", "Ср",
                                                 "Чт",   "Пт", "Сб", "Вс"};

STDataProvider::STDataProvider(QString inName) {
    // Get name
    if (inName.isEmpty() || inName.isNull()) {
        QString path =
                QStandardPaths::writableLocation(QStandardPaths::DataLocation);
        if (!QDir(path).exists()) {
            QDir().mkpath(path);
        }
        inName = path + QDir::separator() + "todo.db";
    } else {
        inName = "todo.db";
    }
    // Open database
    // qDebug() << "Database name:" << inName;

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(inName);
    if (!db.open()) {
        qFatal("%s", db.lastError().text().toStdString().c_str());
    }

    // Alloc vectors
    subjects = new QVector<STSubject*>;
    tasks = new QVector<STTask*>;

    // Init
    initFunc();
}

STDataProvider::~STDataProvider() {
    // Close database
    QSqlDatabase::database().close();

    // Dealoc vectors
    clearQVector<STSubject>(*subjects);
    clearQVector<STTask>(*tasks);
    delete subjects;
    delete tasks;
}

STDataProvider& STDataProvider::getInstance() {
    static STDataProvider singleton;
    return singleton;
}

bool STDataProvider::isEmpty() {
    QSqlQuery query;
    query.prepare("select count(num) from STGlobal;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.next();

    int count = query.value(0).toInt();
    if (count <= 1) {
        return true;
    }
    return false;
}

QString STDataProvider::getDatabaseName() {
    return QSqlDatabase::database().databaseName();
}

int STDataProvider::getVersion() {
    QSqlQuery query;
    query.prepare("select data from STGlobal where num=?;");
    query.addBindValue(version);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.next();

    return query.value(0).toInt();
}


QDate STDataProvider::getBegin() {
    QSqlQuery query;
    query.prepare("select num,data from STGlobal where num>=? and num<=?;");
    query.addBindValue(dBegin);
    query.addBindValue(yBegin);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    int d = -1;
    int m = -1;
    int y = -1;
    while (query.next()) {
        switch (query.value(0).toInt()) {
            case dBegin: {
                d = query.value(1).toInt();
                break;
            }
            case mBegin: {
                m = query.value(1).toInt();
                break;
            }
            case yBegin: {
                y = query.value(1).toInt();
                break;
            }
            default: { qFatal("Unknow num of key"); }
        }
    }

    return QDate(y, m, d);
}

void STDataProvider::setBegin(QDate inDate) {
    QSqlQuery query;
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(dBegin);
    query.addBindValue(inDate.day());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(mBegin);
    query.addBindValue(inDate.month());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(yBegin);
    query.addBindValue(inDate.year());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit globalChanged();
}

QDate STDataProvider::getEnd() {
    QSqlQuery query;
    query.prepare("select num,data from STGlobal where num>=? and num<=?;");
    query.addBindValue(dEnd);
    query.addBindValue(yEnd);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    int d = -1;
    int m = -1;
    int y = -1;
    while (query.next()) {
        switch (query.value(0).toInt()) {
            case dEnd: {
                d = query.value(1).toInt();
                break;
            }
            case mEnd: {
                m = query.value(1).toInt();
                break;
            }
            case yEnd: {
                y = query.value(1).toInt();
                break;
            }
            default: { qFatal("Unknow num of key"); }
        }
    }

    return QDate(y, m, d);
}

void STDataProvider::setEnd(QDate inDate) {
    QSqlQuery query;
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(dEnd);
    query.addBindValue(inDate.day());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(mEnd);
    query.addBindValue(inDate.month());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(yEnd);
    query.addBindValue(inDate.year());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit globalChanged();
}

int STDataProvider::getCountOfWeek() {
    QSqlQuery query;
    query.prepare("select max(week) from STPair;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.next();

    return query.value(0).toInt();
}

QDate STDataProvider::getDateFromWeek(int inWeek, int inDayOfWeek) {
    QDate date = getBegin();

    int week = inWeek;

    if (date.dayOfWeek() != Qt::Monday) {
        while (date.dayOfWeek() != Qt::Monday) {
            if (date.dayOfWeek() == inDayOfWeek && inWeek == 1) {
                return date;
            }
            date = date.addDays(-1);
        }
        week--;
    }

    week--;

    date = date.addDays(7 * week);
    date = date.addDays(inDayOfWeek - 1);

    return date;
}

int STDataProvider::getWeekFromDate(QDate inDate) {
    QDate begin = getBegin();

    if (inDate < begin) {
        return -1;
    }

    int week = 1;
    if (begin.dayOfWeek() != Qt::Monday) {
        int dayOfWeek = begin.dayOfWeek();
        while (dayOfWeek != Qt::Monday) {
            if (begin == inDate) {
                return week;
            }
            begin = begin.addDays(1);
            dayOfWeek = begin.dayOfWeek();
        }

        week++;
    }

    return week + begin.daysTo(inDate) / 7;
}


QVector<STSubject*>* STDataProvider::getSubjects() {
    return deepCopyQVector<STSubject>(*subjects);
}

STSubject* STDataProvider::getSubject(int inId) {
    int index = getIndexById(*subjects, inId);
    return new STSubject(*subjects->at(index));
}

int STDataProvider::addSubject(STSubject inSubject) {
    QSqlQuery query;
    query.prepare("insert into STSubject values (NULL,?,?);");
    query.addBindValue(inSubject.name);
    query.addBindValue(inSubject.nick);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    inSubject.id = query.lastInsertId().toInt();
    subjects->append(new STSubject(inSubject));

    emit subjectsChanged();

    return inSubject.id;
}

void STDataProvider::modSubject(STSubject inSubject) {
    int index = getIndexById(*subjects, inSubject.id);
    delete subjects->at(index);
    subjects->replace(index, new STSubject(inSubject));

    QSqlQuery query;
    query.prepare("update STSubject set name=?, nick=? where id=?;");
    query.addBindValue(inSubject.name);
    query.addBindValue(inSubject.nick);
    query.addBindValue(inSubject.id);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit subjectsChanged();
}

void STDataProvider::delSubject(STSubject inSubject) {
    delSubject(inSubject.id);
}

void STDataProvider::delSubject(int inId) {
    if (inId == -1) {//INVALID_ID
        qDebug() << "Delete invalid id";
        return;
    }

    int index = getIndexById(*subjects, inId);
    delete subjects->at(index);
    subjects->remove(index);

    QSqlQuery query;
    query.prepare("delete from STSubject where id=?;");
    query.addBindValue(inId);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

QVector<STTask*>* STDataProvider::getTaskOnDate(QDate inDate) {
    QVector<STTask*> ret;
    STTask* task;
    for (int i = 0; i < tasks->size(); i++) {
        task = tasks->at(i);
        if (task->date == inDate) {
            ret.append(task);
        }
    }

    return deepCopyQVector<STTask>(ret);
}

QVector<QVector<STTask*>*>* STDataProvider::getTaskOnWeekNumber(
        int inWeekNumber) {
    QVector<QVector<STTask*>*> ret;
    for (int i = Qt::Monday; i <= Qt::Sunday; i++) {
        ret.append(getTaskOnDate(getDateFromWeek(inWeekNumber, i)));
    }

    return deepCopyQVector<QVector<STTask*> >(ret);
}

QVector<STTask*>* STDataProvider::getAllTasks() {
    return deepCopyQVector<STTask>(*tasks);
}

QVector<STTask*>* STDataProvider::getOutdatedTasks() {
    QVector<STTask*> ret;
    STTask* task;
    QDate currentDate = QDate::currentDate();
    for (int i = 0; i < tasks->size(); i++) {
        task = tasks->at(i);
        if (task->date < currentDate && !task->isComplete) {
            ret.append(task);
        }
    }

    return deepCopyQVector<STTask>(ret);
}

STTask* STDataProvider::getTask(int inId) {
    int index = getIndexById(*tasks, inId);
    return new STTask(*tasks->at(index));
}

int STDataProvider::addTask(STTask inTask) {
    QSqlQuery query;
    query.prepare("PRAGMA foreign_keys = ON;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert into STTask values (NULL,?,?,?,?,?,?,?,?,?);");
    query.addBindValue(getVariantFromId(inTask.subjectId));
    query.addBindValue(inTask.seriesNumber);
    query.addBindValue(inTask.isComplete);
    query.addBindValue(inTask.date.day());
    query.addBindValue(inTask.date.month());
    query.addBindValue(inTask.date.year());
    query.addBindValue(inTask.task);
    query.addBindValue(inTask.description);
    query.addBindValue(inTask.prior);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    inTask.id = query.lastInsertId().toInt();
    tasks->append(new STTask(inTask));

    emit tasksChanged();

    return inTask.id;
}

void STDataProvider::modTask(STTask inTask) {
    int index = getIndexById(*tasks, inTask.id);
    delete tasks->at(index);
    tasks->replace(index, new STTask(inTask));

    QSqlQuery query;
    query.prepare(
                "update STTask set subjectId=?, seriesNumber=?, isComplete=?, dateD=?, "
                "dateM=?, dateY=?, task=?, description=?, prior=? where id=?;");
    query.addBindValue(getVariantFromId(inTask.subjectId));
    query.addBindValue(inTask.seriesNumber);
    query.addBindValue(inTask.isComplete);
    query.addBindValue(inTask.date.day());
    query.addBindValue(inTask.date.month());
    query.addBindValue(inTask.date.year());
    query.addBindValue(inTask.task);
    query.addBindValue(inTask.description);
    query.addBindValue(inTask.prior);
    query.addBindValue(inTask.id);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit tasksChanged();
}

void STDataProvider::delTask(STTask inTask) { delTask(inTask.id); }

void STDataProvider::delTask(int inId) {
    if (inId == -1) {//INVALID_ID
        qDebug() << "Delete invalid id";
        return;
    }

    int index = getIndexById(*tasks, inId);
    delete tasks->at(index);
    tasks->remove(index);

    QSqlQuery query;
    query.prepare("delete from STTask where id=?;");
    query.addBindValue(inId);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

int STDataProvider::getNewSeriesNumber() {
    QSqlQuery query;
    query.prepare("select max(seriesNumber) from STTask;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.next();

    return query.value(0).toInt() + 1;
}

void STDataProvider::delTaskSeries(int inSeries) {
    STTask* task;
    for (int i = 0; i < tasks->size(); i++) {
        task = tasks->at(i);
        if (task->seriesNumber == inSeries) {
            delete task;
            tasks->remove(i);
            i = 0;
            // WARNING: bad method i=0
        }
    }

    QSqlQuery query;
    query.prepare("delete from STTask where seriesNumber=?;");
    query.addBindValue(inSeries);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

void STDataProvider::importDatabase(QString inFileName) {
    if (inFileName.isEmpty() || inFileName.isNull()) {
        return;
    }

    QString dbName = getDatabaseName();

    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query;
    query.clear();
    db.close();

    if (!QFile::remove(dbName)) {
        QMessageBox::warning(
                    NULL, QString::fromUtf8("Ошибка"),
                    QString::fromUtf8("Не удалось удалить файл со старыми данными"));
        return;
    }

    if (!QFile::copy(inFileName, dbName)) {
        // remove was ok, database is clear :(
        QMessageBox::critical(
                    NULL, QString::fromUtf8("Ошибка"),
                    QString::fromUtf8("Не удалось скопировать файл с новыми данными"));
        return;
    }

    db.setDatabaseName(dbName);
    if (!db.open()) {
        qFatal("%s", db.lastError().text().toStdString().c_str());
    }

    initFunc();

    emit globalChanged();
    emit subjectsChanged();
    emit tasksChanged();
    emit updated();
}

void STDataProvider::initFunc() {
    //  Enable foreign
    QSqlQuery query;
    query.prepare("PRAGMA foreign_keys = ON;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    //  Create all tables, if they not exist
    query.clear();
    query.prepare(DP_CREATE_GLOBAL);
    if (!query.exec()) {
         DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare(DP_CREATE_SUBJECT);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare(DP_CREATE_TASK);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    //  Set version
    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(version);
    query.addBindValue(1);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

void STDataProvider::fillFunc() {
    //  Fill vectors
    QSqlQuery query;

    query.clear();
    query.prepare("select * from STSubject;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }
    clearQVector<STSubject>(*subjects);
    STSubject* subject;
    while (query.next()) {
        subject = new STSubject;
        subject->id = query.value(0).toInt();
        subject->name = query.value(1).toString();
        subject->nick = query.value(2).toString();
        subjects->append(subject);
    }

    query.clear();
    query.prepare("select * from STTask;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }
    clearQVector<STTask>(*tasks);
    STTask* task;
    while (query.next()) {
        task = new STTask;
        task->id = query.value(0).toInt();
        task->subjectId = getIdFromVariant(query.value(1));
        task->seriesNumber = query.value(2).toInt();
        task->isComplete = query.value(3).toBool();
        task->date = QDate(query.value(6).toInt(), query.value(5).toInt(),
                           query.value(4).toInt());
        task->task = query.value(7).toString();
        task->description = query.value(8).toString();
        task->prior = query.value(9).toInt();
        tasks->append(task);
    }

    query.clear();

    emit globalChanged();
    emit subjectsChanged();
    emit tasksChanged();
}

int STDataProvider::getIdFromVariant(QVariant inVar) {
    if (inVar.isNull() ||
            (inVar.type() != QVariant::Int && inVar.type() != QVariant::LongLong)) {
        return -1;
    }
    return inVar.toInt();
}

QVariant STDataProvider::getVariantFromId(int inId) {
    if (inId == -1) {
        return QVariant(QVariant::Int);
    }
    return QVariant(inId);
}
