#ifndef FILTERELEMENT_H
#define FILTERELEMENT_H

#include "st.include.h"

class STFilterElement {
    public:
        STFilterElement();
        bool showCompleted;
        QVector<int> unselectedSubjects;
};

#endif // FILTERELEMENT_H
