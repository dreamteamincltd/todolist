#ifndef CALENDAR_H
#define CALENDAR_H

#include "st.include.h"

#define W_CALENDAR_DATE "StodoDate"  // It's need for store user data in label
#define W_CALENDAR_DURATION 300

#define BORDER_WIDTH 2

#define MONTH_TITLE 35
#define WEEK_DAYS_HEIGHT 22
#define CALENDAR_CELL_SIZE 31

#define WEEK_SELECTOR_HEIGHT 32
#define WEEK_SELECTOR_WIDTH 223

namespace Ui {
    class WCalendar;
}

class WCalendar : public QWidget {
        Q_OBJECT

    signals:
        void sigIncMonth();
        void sigDecMonth();
        void selectedDateChanged(QDate inDate);

    public:
        explicit WCalendar(QWidget *parent = 0);
        ~WCalendar();

    public slots:
        void fillCalendarOnDate(QDate inDate);

    protected:
        void mousePressEvent(QMouseEvent *ev);

    private:
        static const int HEADER_ROW;
        static const int FIRST_ROW;
        static const int COUNT_OF_ROW;
        static const QString CSS_HEADER;
        static const QString CSS_OTHER_MONTH;
        static const QString CSS_CURRENT_MONTH;
        static const QString CSS_SELECTED_DAY;
        static const QString CSS_TODAY;

        Ui::WCalendar *ui;
        QDate selectedDate;
        bool isInitialized;

        void setupUiForm();

    private slots:
        void slotIncMonth();
        void slotDecMonth();
};


#endif // CALENDAR_H
